/**
 * 
 */
package com.state.interview.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.state.interview.Randomizer;

/**
 * @author kolivero
 *
 */
public class RandomizerTest {
	
	private Randomizer randomizer = null; 
	
	@Before
	public void setup(){
		randomizer = new Randomizer();
	}

	@Test
	public void testNumberOfNumbersMatch(){
		Integer[] test= randomizer.getRandomIntegers(10);
		
		Assert.assertTrue(test.length==10);
	}
	
	@Test
	public void testAllNumberPositive(){
		Integer[] test= randomizer.getRandomIntegers(100);
		
		for (int i = 0; i < test.length; i++) {
			Assert.assertTrue(test[i]>0);
		}
		
	}
	
	@Test
	public void testRandomNumberIsPositive(){
		Integer test= randomizer.getRandomInteger();
		
		Assert.assertTrue(test >0);
	}
}
