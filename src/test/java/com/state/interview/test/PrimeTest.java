/**
 * 
 */
package com.state.interview.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.state.interview.Prime;

/**
 * @author kolivero
 *
 */
public class PrimeTest {
	
	private Prime prime = null; 
	
	@Before
	public void setup(){
		prime = new Prime();
	}

	@Test
	public void testPrimeNumbers(){
		
		Assert.assertTrue(prime.isPrime(3));
		Assert.assertTrue(prime.isPrime(7));
		Assert.assertTrue(prime.isPrime(11));
		Assert.assertTrue(prime.isPrime(17));
		Assert.assertTrue(prime.isPrime(199));
	}
	
	@Test
	public void testNonPrimeNumbers(){
		
		Assert.assertFalse(prime.isPrime(2));
		Assert.assertFalse(prime.isPrime(4));
		Assert.assertFalse(prime.isPrime(10));
		Assert.assertFalse(prime.isPrime(100));
		Assert.assertFalse(prime.isPrime(200));
	}
	
}
