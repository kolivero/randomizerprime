/**
 * 
 */
package com.state.interview;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import com.state.interview.MainApp.PrimeCheckResult;

/**
 * @author kolivero
 *
 */
public class Randomizer implements Runnable {

	public void run() {

	}

	private Integer maxInteger;

	private static final Random random = new Random();

	public Randomizer() {
		// by default set to 999 for simplicity of this test
		this.maxInteger = 999;
	}

	/**
	 * Use this constructor to override the max integer
	 * 
	 * @param maxInteger
	 */
	public Randomizer(Integer maxInteger) {
		this.maxInteger = maxInteger;
	}

	/**
	 * Get an array of random positive integers
	 */
	public Integer[] getRandomIntegers(int size) {
		// using a list to keep insertion orders
		List<Integer> result = new ArrayList<Integer>();
		for (int i = 0; i < size; i++) {
			result.add(getRandomInteger()); // +1 because i don't want 0 to be
											// returned

		}
		return result.toArray(new Integer[result.size()]);
	}

	/*
	 * This method can be used to get a single random integer
	 */
	public Integer getRandomInteger() {
		return (random.nextInt(maxInteger - 1) + 1);
	}
	
	/**
	 * The idea of having a separate class is to separate the processing behavior 
	 * from the business logic. 
	 * @author kolivero
	 *
	 */
	public static class RandomizerJob implements Runnable {
		//Explicitly final because the instance shoudn't be modified once set
		private final BlockingQueue<Integer> requestQueue;
		private final BlockingQueue<PrimeCheckResult> responseQueue;
		private final Randomizer randomizer;
		private int size;
		// This is a bit overboard, since on the threadpool there is only 1 thread for each job
		//But the purpose is guaranty there will be jost one thread setting the job as done
		private AtomicBoolean done = new AtomicBoolean(false); 

		/**
		 * randomizer job is responsible to send integers to prime and then
		 * check the results.
		 * 
		 * @param requestQueue
		 * @param responseQueue
		 */
		public RandomizerJob(int size, int maxInteger, final BlockingQueue<Integer> requestQueue,
				final BlockingQueue<PrimeCheckResult> responseQueue) {
			this.requestQueue = requestQueue;
			this.responseQueue = responseQueue;
			randomizer = new Randomizer(maxInteger);
			this.size = size;
		}

		public void run() {
			//Get the serie of intergers to be sent 
			Integer[] numbers = this.randomizer.getRandomIntegers(this.size);
			//randomizer main job is to get sequence of numbers and send it to the Prime for check
			for (int i = 0; i < numbers.length; i++) {
				try {
					//send numbers to the Prime for check
					requestQueue.put(numbers[i]);
				} catch (InterruptedException e) {
					System.err.println("There was a error trying to add numbers to the queue. Details: "+ e.getMessage());
				}
			}
			
			int replyCount = 0;
			// once we send the numbers, we should wait for response to come and print it 
			// We should expect same number of response received.
			while(!Thread.currentThread().isInterrupted() && replyCount < size){
				try {
					PrimeCheckResult result= responseQueue.take();
					System.out.println("Randomizer - The number "+ result.getNumber() + ", prime ckeck is: " + result.isPrimeCheck());
					replyCount++; // count the number of replies received
				} catch (InterruptedException e) {
					System.err.println("There was a error waiting for response numbers. Details: "+ e.getMessage());
				} 
				
			}
			//mark the job as done 
			System.out.println("Randomizer - We have received all responses - done");
			//Mark job as done
			done.set(true);

		}
	
	  public boolean isDone() {
		return done.get();
	}
	
	}

}
