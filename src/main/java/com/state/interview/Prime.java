/**
 * 
 */
package com.state.interview;

import java.util.concurrent.BlockingQueue;

import com.state.interview.MainApp.PrimeCheckResult;

/**
 * @author kolivero
 *
 */
public class Prime {
	

	/**
	 * Check if a number is a prime number
	 * 
	 * @param number
	 * @return
	 */
	public boolean isPrime(int number) {
		// if nnumber is divisible by 2 it's not prime
		if (number % 2 == 0) {
			return false;
		}

		// if not divisible by 2, check up to the square of the number
		// i+=2 to check only odds number for less iterations
		for (int i = 3; i * i <= number; i += 2) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	
	public static class PrimeJob implements Runnable {

			private final BlockingQueue<Integer> requestQueue;
			private final BlockingQueue<PrimeCheckResult> responseQueue;
			private final Prime prime = new Prime();
			/**
			 * randomizer job is responsible to send integers to prime and then
			 * check the results.
			 * 
			 * @param requestQueue
			 * @param responseQueue
			 */
			public PrimeJob(final BlockingQueue<Integer> requestQueue,
					final BlockingQueue<PrimeCheckResult> responseQueue) {
				this.requestQueue = requestQueue;
				this.responseQueue = responseQueue;
			}

			public void run() {
				while(!Thread.currentThread().isInterrupted()){
					try {
						Integer number= requestQueue.take();
					    PrimeCheckResult prc = new PrimeCheckResult(number, prime.isPrime(number)); 
					    responseQueue.put(prc);
					} catch (InterruptedException e) {
						System.err.println("There was a error waiting for request numbers. Details: "+ e.getMessage());
					} 
					
				}

			}
		
	}
}
