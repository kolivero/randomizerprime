/**
 * 
 */
package com.state.interview;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.state.interview.Prime.PrimeJob;
import com.state.interview.Randomizer.RandomizerJob;

/**
 * @author kolivero
 *
 */
public class MainApp {
	
	// Executor service easy the thread management: start/stop of threads without dealing with the 
	// Thread class itself 
	private static ExecutorService randomizerExecService = Executors.newFixedThreadPool(2);
	//Blocking queues were added in java 1.5. It eases the thread safty part when working 
	//with multi-threading applications. 
	//I'm choosing this Interface to avoid doing manual wait/notifyall routines that could result
	// Inefficient and unsafe. Since i want the communication between the 2 treads to be fast, 
	// and i think this is the best data structure for it.
	private static final BlockingQueue<Integer> requestQueue = new ArrayBlockingQueue<Integer>(9999);
	
	//There are 2 different queues instances. One for requests and nother for response. 
	private static final BlockingQueue<PrimeCheckResult> responseQueue = new ArrayBlockingQueue<PrimeCheckResult>(9999);
	/**
	 * Main app is responsible for initialzing both jobs
	 * @param args
	 */
	public static void main(String[] args) {
		
		// We work directly with the jobs
		//100 numbers  999 the biggest number in the sequence
		RandomizerJob randomJob = new RandomizerJob(100, 999, requestQueue, responseQueue);
		PrimeJob primeJob = new PrimeJob(requestQueue, responseQueue);
		
		//I could just have done Thread.start, but i prefer using executors
		randomizerExecService.execute(randomJob);
		randomizerExecService.execute(primeJob);
		
		//Keep waiting until random notifies the task have been completed
		while(!randomJob.isDone()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.err.println("There was an error while waiting to complete the tasks. Details: "+ e.getMessage());
			}
		}
		
		//when task are complited just shutdown the pool
		randomizerExecService.shutdown();
		System.exit(0);
	}
	
	public static class PrimeCheckResult{
		
		private int number; 
		private boolean primeCheck; 
		
		public PrimeCheckResult(Integer number, boolean primeCheck) {
			this.number = number; 
			this.primeCheck = primeCheck;
		}
		
		public int getNumber() {
			return number;
		}
		
		public boolean isPrimeCheck() {
			return primeCheck;
		}
	}

}
