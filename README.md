# README #

### What is this repository for? ###

* Simple application that comunicate 2 components that run on separate threads  
  through the use of Queues. 
  Randomizer‘s job is to generate a series of positive random integers and send   
  those to Prime via a distributed queue of integers. 
   Primes job is to receive the integers and calculate whether the integer is a  
   prime or not and return the answer to Randomizer via a distributed queue that  
   contains the original number and a Boolean; which Randomizer will print to  
   system out.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* Kelvis Olivero